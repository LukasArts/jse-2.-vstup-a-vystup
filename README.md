Zadání
======
## 1. část
Společnost vlastní několik budov, které jsou označovány jako EBC Meeting Centres a ty obsahují zasedací místnosti, které dále v textu budeme nazývat Meeting Rooms. Implementujte aplikaci, která umožní zpracovávat nastavení Meeting Centres a v nich Meeting Rooms.
 
### Objekty

#### Meeting Centre
`name`
: je název budovy (řetězec o délce 2..100 znaků),

`code`
: unikátní identifikátor budovy (řetězec o délce 5..50 znaků, který může obsahovat velká a malá písmena anglické abecedy a znaky: tečka, dvojtečka a podtržítko),

`description`
: slovní popis budovy (řetězec o délce 10..300 znaků),

`meetingRooms`
: Meeting Centre může obsahovat 0..n MeetingRooms.

#### Meeting Room
`name`
: je název místnosti (řetězec o délce 2..100 znaků),

`code`
: unikátní identifikátor místnosti (řetězec o délce 5..50 znaků, který může obsahovat velká a malá písmena anglické abecedy a znaky: tečka, dvojtečka a podtržítko),

`description`
: je slovní popis budovy (řetězec o délce 10..300 znaků),

`capacity`
: maximální počet osob, který se pohodlně vejde do místnosti (celé číslo v rozsahu 1..100),

`videoConference`
: informace zdali místnost obsahuje zařízení pro videokonferenci (ano/ne),

`meetingCentre`
: Meeting Room patří právě do jednoho Meeting Centre.

### Vstupní data

Soubor s daty je ve formátu [CSV](https://tools.ietf.org/html/rfc4180), přičemž platí následující:

- První řádek obsahuje řetězec MEETING_CENTRES.
- Následuje 1..n řádků s informacemi o jednotlivých budovách. Tyto informace jsou na řádku v následujícím pořadí: Name,Code,Description.
- Následuje řádek, který obsahuje řetězec MEETING_ROOMS.
- Následuje 1..m řádků s informacemi o jednotlivých zasedacích místnostech. Tyto informace jsou na řádku v následujícím pořadí: Name,Code,Description,Capacity,VideoConference,MeetingCentreCode(kód Meeting Centre, do kterého Meeting Room patří).
- Data na řádku jsou oddělena čárkou, přičemž samotná data nebudou obsahovat čárku.
- Používá kódování UTF-8.

## 2. část
Implementujete část, která umožní plánovat jednotlivé rezervace místností. Tato část si bude svá data ukládat do [XML](https://www.w3.org/TR/xml/) souboru (jiná aplikace z historických důvodů potřebuje tento formát) a zároveň umožní exportovat naplánované rezervace do formátu [JSON](https://tools.ietf.org/html/rfc7159).

### Objekty

#### Správa rezervací
`MeetingRoom`
: jedna rezervace je vždy naplánována na právě jednu místnost.

`Date`
: rezervace je vždy naplánována na jeden konkrétní den. Pro zjednodušení předpokládejte, že rezervace nemůže být naplánována na více dní.

`TimeFrom` – `TimeTo`
: čas „od kdy do kdy“ je rezervace (např. od 10:00 do 11:30).

`ExpectedPersonsCount`
: očekávaný počet osob (celé číslo v rozsahu 1..kapacita místnosti).

`Customer`
: jméno zákazníka (řetězec v rozsahu 2..100 znaků).

`VideoConference`
: zdali chce zákazník připravit zařízení na video konferenci (pouze pro místnosti, která umožňují video konferenci).

`Note`
: poznámka k rezervaci (řetězec v rozsahu 0..300 znaků).