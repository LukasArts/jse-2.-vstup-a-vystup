package cz.unicorncollege.bt.model;

import java.time.LocalDate;
import java.time.LocalTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

public class MeetingRoom extends MeetingObject {
    final Comparator<Reservation> byFromTime = Comparator.comparing(o -> LocalTime.parse(o.getTimeFrom()));
    final Comparator<Reservation> byFromDate = Comparator.comparing(Reservation::getDate);

    private int capacity;
    private boolean hasVideoConference;
    private MeetingCentre meetingCentre;
    private List<Reservation> reservations;

    public int getCapacity() {
        return capacity;
    }

    public void setCapacity(int capacity) {
        this.capacity = capacity;
    }

    public boolean isHasVideoConference() {
        return hasVideoConference;
    }

    public void setHasVideoConference(boolean hasVideoConference) {
        this.hasVideoConference = hasVideoConference;
    }

    public MeetingCentre getMeetingCentre() {
        return meetingCentre;
    }

    public void setMeetingCentre(MeetingCentre meetingCentre) {
        this.meetingCentre = meetingCentre;
    }

    public List<Reservation> getReservations() {
        return reservations;
    }

    public void setReservations(List<Reservation> reservations) {
        this.reservations = reservations;
    }

    public List<Reservation> addReservation(Reservation reservation) {
        reservations.add(reservation);
        return reservations;
    }

    public List<Reservation> delReservation(Reservation reservation) {
        reservations.remove(reservation);
        return reservations;
    }

    public List<Reservation> getSortedReservationsByDate(Date date) {
        if (reservations != null) {
            LocalDate localDate = date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
            List<Reservation> todayReservations = new ArrayList<>();
            for (Reservation r : reservations)
                if (localDate.equals(r.getDate()))
                    todayReservations.add(r);
            todayReservations.sort(byFromTime);
            return todayReservations;
        }
        return null;
    }

    public List<Reservation> getSortedReservations() {
        if (reservations != null) {
            List<Reservation> result = reservations;
            result.sort(byFromDate.thenComparing(byFromTime));
            return result;
        }
        return null;
    }

    public String toCSVRow() {
        return name +
                ',' +
                code +
                ',' +
                description +
                ',' +
                capacity +
                ',' +
                (hasVideoConference ? "YES" : "NO") +
                ',' +
                meetingCentre.getCode();
    }

    public String description() {
        // At the moment it is just an alias for toCSVRow() method. Shall there be need for fancier description, this
        // is the place to modify.
        return toCSVRow();
    }
}
