package cz.unicorncollege.bt.model;

import java.time.LocalDate;

public class Reservation {
    private MeetingRoom meetingRoom;
    private LocalDate date;
    private String timeFrom;
    private String timeTo;
    private int expectedPersonCount;
    private String customer;
    private boolean needVideoConference;
    private String note;

    public MeetingRoom getMeetingRoom() {
        return meetingRoom;
    }

    public void setMeetingRoom(MeetingRoom meetingRoom) {
        this.meetingRoom = meetingRoom;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public String getFormattedDate() {
        return timeFrom +
                " – " +
                timeTo;
    }

    public String getTimeFrom() {
        return timeFrom;
    }

    public void setTimeFrom(String timeFrom) {
        this.timeFrom = timeFrom;
    }

    public String getTimeTo() {
        return timeTo;
    }

    public void setTimeTo(String timeTo) {
        this.timeTo = timeTo;
    }

    public int getExpectedPersonCount() {
        return expectedPersonCount;
    }

    public void setExpectedPersonCount(int expectedPersonCount) {
        this.expectedPersonCount = expectedPersonCount;
    }

    public String getCustomer() {
        return customer;
    }

    public void setCustomer(String customer) {
        this.customer = customer;
    }

    public boolean isNeedVideoConference() {
        return needVideoConference;
    }

    public void setNeedVideoConference(boolean needVideoConference) {
        this.needVideoConference = needVideoConference;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String shortDesc() {
        return meetingRoom.getName() +
                ' ' +
                date +
                " (" +
                timeFrom +
                '–' +
                timeTo +
                ')';
    }
}
