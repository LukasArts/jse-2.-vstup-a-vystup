package cz.unicorncollege.bt.model;

import java.util.Comparator;
import java.util.List;

public class MeetingCentre extends MeetingObject {
    final Comparator<MeetingRoom> byCode = Comparator.comparing(MeetingObject::getCode);
    private List<MeetingRoom> meetingRooms;

    public List<MeetingRoom> getMeetingRooms() {
        return meetingRooms;
    }

    public void setMeetingRooms(List<MeetingRoom> meetingRooms) {
        this.meetingRooms = meetingRooms;
    }

    public List<MeetingRoom> getMeetingRoomsSortedByCode() {
        if (meetingRooms != null) {
            List<MeetingRoom> result = meetingRooms;
            result.sort(byCode);
            return result;
        }
        return null;
    }

    public String toCSVRow() {
        return name +
                ',' +
                code +
                ',' +
                description +
                ",,,";
    }

    public String shortDesc() {
        return '\t' + String.format("%-15s", code) +
                name;
    }

    public String longDesc() {
        return code + ", " + name + ", " + description + " (" + meetingRooms.size() + " rooms)";
    }

    public String listMeetingRooms() {
        // 1.1 Alpha,EBC-C7-MR:1_1,Standardní zasedací místnost,8,NO,EBC-MC_C7
        StringBuilder sb = new StringBuilder();
        for (MeetingRoom mr : meetingRooms) {
            sb.append(mr.description()).append('\n');
        }
        return sb.toString();
    }

    public MeetingCentre addMeetingRoom(MeetingRoom meetingRoom) {
        meetingRooms.add(meetingRoom);
        return this;
    }

    public MeetingCentre removeMeetingRoom(MeetingRoom meetingRoom) {
        meetingRooms.remove(meetingRoom);
        return this;
    }
}
