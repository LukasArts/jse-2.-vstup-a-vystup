package cz.unicorncollege.bt.utils;

import cz.unicorncollege.bt.model.MeetingCentre;
import cz.unicorncollege.bt.model.MeetingRoom;
import cz.unicorncollege.bt.model.Reservation;
import cz.unicorncollege.controller.MeetingController;
import cz.unicorncollege.controller.ReservationController;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import javax.json.*;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.*;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.stream.Collectors;

public class FileParser {
    private static final String defaultDbFile = "db/programDb.csv";

    /**
     * Method to import data from the chosen file.
     */
    public static List<MeetingCentre> importData() {

        String locationFilter = Choices.getInput("Enter path of imported file: ");

        File f = new File(locationFilter);
        if (!f.exists() || f.isDirectory()) {
            System.out.println("File " + locationFilter + " is not a valid file.");
            return null;
        }

        List<MeetingCentre> allMeetingCentres = loadDataFromFile(locationFilter);

        System.out.println();

        System.out.println("**************************************************");
        System.out.println("Data was imported. " + allMeetingCentres.size() + " objects of MeetingCentres was loaded");
        System.out.println("**************************************************");

        System.out.println();

        return allMeetingCentres;
    }

    /**
     * Method to save the data to file.
     */
    public static void saveData(String output) {

        try (BufferedWriter bw = new BufferedWriter(new FileWriter(defaultDbFile))) {
            bw.write(output);
        } catch (IOException e) {
            e.printStackTrace();
        }

        System.out.println();

        System.out.println("**************************************************");
        System.out.println("Data was saved correctly.");
        System.out.println("**************************************************");

        System.out.println();
    }

    /**
     * Method to save the data to file.
     */
    public static void saveData(MeetingController controll) {
        File fileToSaveXML = new File("db/db.xml");

        try {
            DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder docBuilder = docFactory.newDocumentBuilder();

            // root elements
            Document doc = docBuilder.newDocument();
            Element rootElement = doc.createElement("EBC");
            doc.appendChild(rootElement);

            // meeting centres
            Element mcsElement = doc.createElement("meetingCenters");
            for (MeetingCentre mc : controll.getMeetingCentres()) {
                Element mcElement = doc.createElement("meetingCenter");
                Element mcName = doc.createElement("name");
                mcName.appendChild(doc.createTextNode(mc.getName()));
                mcElement.appendChild(mcName);

                Element mcCode = doc.createElement("code");
                mcCode.appendChild(doc.createTextNode(mc.getCode()));
                mcElement.appendChild(mcCode);

                Element mcDescription = doc.createElement("description");
                mcDescription.appendChild(doc.createTextNode(mc.getDescription()));
                mcElement.appendChild(mcDescription);

                // meeting rooms
                Element meetingRooms = doc.createElement("meetingRooms");
                for (MeetingRoom mr : mc.getMeetingRooms()) {
                    Element mrElement = doc.createElement("meetingRoom");
                    Element mrName = doc.createElement("name");
                    mrName.appendChild(doc.createTextNode(mr.getName()));
                    mrElement.appendChild(mrName);

                    Element mrCode = doc.createElement("code");
                    mrCode.appendChild(doc.createTextNode(mr.getCode()));
                    mrElement.appendChild(mrCode);

                    Element mrDescription = doc.createElement("description");
                    mrDescription.appendChild(doc.createTextNode(mr.getDescription()));
                    mrElement.appendChild(mrDescription);

                    Element mrCapacity = doc.createElement("capacity");
                    mrCapacity.appendChild(doc.createTextNode(String.valueOf(mr.getCapacity())));
                    mrElement.appendChild(mrCapacity);

                    Element mrVideo = doc.createElement("hasVideoConference");
                    mrVideo.appendChild(doc.createTextNode(String.valueOf(mr.isHasVideoConference())));
                    mrElement.appendChild(mrVideo);

                    // reservations
                    Element reservations = doc.createElement("reservations");
                    if (mr.getReservations() != null)
                        for (Reservation r : mr.getSortedReservations()) {
                            Element reservation = doc.createElement("reservation");

                            Element date = doc.createElement("date");
                            date.appendChild(doc.createTextNode(r.getDate().toString()));
                            reservation.appendChild(date);

                            Element timeFrom = doc.createElement("timeFrom");
                            timeFrom.appendChild(doc.createTextNode(r.getTimeFrom()));
                            reservation.appendChild(timeFrom);

                            Element timeTo = doc.createElement("timeTo");
                            timeTo.appendChild(doc.createTextNode(r.getTimeTo()));
                            reservation.appendChild(timeTo);

                            Element expectedPersonsCount = doc.createElement("expectedPersonsCount");
                            expectedPersonsCount.appendChild(doc.createTextNode(String.valueOf(r.getExpectedPersonCount())));
                            reservation.appendChild(expectedPersonsCount);

                            Element customer = doc.createElement("customer");
                            customer.appendChild(doc.createTextNode(r.getCustomer()));
                            reservation.appendChild(customer);

                            Element videoConference = doc.createElement("videoConference");
                            videoConference.appendChild(doc.createTextNode(String.valueOf(r.isNeedVideoConference())));
                            reservation.appendChild(videoConference);

                            Element note = doc.createElement("note");
                            note.appendChild(doc.createTextNode(r.getNote()));
                            reservation.appendChild(note);

                            reservations.appendChild(reservation);
                        }

                    mrElement.appendChild(reservations);

                    meetingRooms.appendChild(mrElement);
                }

                mcElement.appendChild(meetingRooms);


                mcsElement.appendChild(mcElement);

            }
            rootElement.appendChild(mcsElement);


            // write the content into xml file
            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();
            DOMSource source = new DOMSource(doc);
            StreamResult result = new StreamResult(fileToSaveXML);

            // Output to console for testing
            // StreamResult result = new StreamResult(System.out);

            transformer.transform(source, result);

            System.out.println("File saved!");

        } catch (ParserConfigurationException | TransformerException pce) {
            pce.printStackTrace();
        }


        System.out.println();

        System.out.println("**************************************************");
        System.out.println("Data was saved correctly.");
        System.out.println("**************************************************");

        System.out.println();
    }


    /**
     * Method to export data to JSON file
     *
     * @param controllReservation Object of reservation controller to get all reservation and
     *                            other data if needed
     */
    public static void exportDataToJSON(ReservationController controllReservation) {
        String locationFilter = Choices.getInput("Enter name of the file for export: ");

        File exportDataFile = new File(locationFilter);

        JsonBuilderFactory factory = Json.createBuilderFactory(null);

        JsonObjectBuilder job = factory.createObjectBuilder()
                .add("schema", "PLUS4U.EBC.MCS.MeetingRoom_Schedule_1.0")
                .add("uri", "ues:UCL-BT:UCL.INF/DEMO_REZERVACE:EBC.MCS.DEMO/MR001/SCHEDULE");

        JsonArrayBuilder data = factory.createArrayBuilder();

        // each meeting center
        for (MeetingCentre mc : controllReservation.getMeetingController().getMeetingCentresSortedByCode()) {

            // each meeting room
            for (MeetingRoom mr : mc.getMeetingRoomsSortedByCode()) {
                List<Reservation> sortedReservations = mr.getSortedReservations();
                if (sortedReservations == null || sortedReservations.size() == 0)
                    continue;

                Map<LocalDate, List<Reservation>> reservationsByDate = new TreeMap<>();
                // transforms reservations list to date => reservations map
                reservationsByDate = sortedReservations.stream().collect(Collectors.groupingBy(Reservation::getDate));

                // each day
                JsonObjectBuilder days = factory.createObjectBuilder();
                for (Map.Entry<LocalDate, List<Reservation>> entry : reservationsByDate.entrySet()) {

                    // each reservation
                    JsonArrayBuilder day = factory.createArrayBuilder();
                    for (Reservation r : entry.getValue()) {
                        JsonObject reservation = factory.createObjectBuilder()
                                .add("from", r.getTimeFrom())
                                .add("to", r.getTimeTo())
                                .add("expectedPersonsCount", r.getExpectedPersonCount())
                                .add("customer", r.getCustomer())
                                .add("videoConference", r.isNeedVideoConference())
                                .add("note", r.getNote()).build();
                        day.add(reservation);
                    }
                    days.add(entry.getKey().format(DateTimeFormatter.ofPattern("dd.MM.yyyy")), day);
                }
                JsonObject item = factory.createObjectBuilder()
                        .add("meetingCentre", mc.getCode())
                        .add("meetingRoom", mr.getCode())
                        .add("reservations", days).build();
                data.add(item);
            }
        }
        job.add("data", data);


        try (FileWriter fileWriter = new FileWriter(exportDataFile, false)) {
            fileWriter.write(job.build().toString());
            fileWriter.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }


        System.out.println();

        if (exportDataFile != null) {
            System.out.println("**************************************************");
            System.out.println("Data was exported correctly. The file is here: " + exportDataFile.getAbsolutePath());
            System.out.println("**************************************************");
        } else {
            System.out.println("**************************************************");
            System.out.println("Something terrible happend during exporting!");
            System.out.println("**************************************************");
        }

        System.out.println();
    }

    /**
     * Method to load the data from file.
     *
     * @return
     */
    public static List<MeetingCentre> loadDataFromFile() {
        List<MeetingCentre> meetingCentreList = new ArrayList<>();

        try {
            File fXmlFile = new File("db/db.xml");
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.parse(fXmlFile);

            doc.getDocumentElement().normalize();

            System.out.println("Root element: " + doc.getDocumentElement().getNodeName());

            NodeList meetingCenters = doc.getElementsByTagName("meetingCenter");

            for (int i = 0; i < meetingCenters.getLength(); i++) {
                Node mcNode = meetingCenters.item(i);

                MeetingCentre meetingCentre = new MeetingCentre();

                Element mcElement = (Element) mcNode;
                meetingCentre.setName(mcElement.getElementsByTagName("name").item(0).getTextContent());
                meetingCentre.setCode(mcElement.getElementsByTagName("code").item(0).getTextContent());
                meetingCentre.setDescription(mcElement.getElementsByTagName("description").item(0).getTextContent());

                meetingCentreList.add(meetingCentre);
                meetingCentre.setMeetingRooms(new ArrayList<>());

                NodeList meetingRooms = mcElement.getElementsByTagName("meetingRoom");

                for (int j = 0; j < meetingRooms.getLength(); j++) {
                    Node mrNode = meetingRooms.item(j);

                    MeetingRoom meetingRoom = new MeetingRoom();
                    meetingRoom.setMeetingCentre(meetingCentre);

                    Element mrElement = (Element) mrNode;
                    meetingRoom.setName(mrElement.getElementsByTagName("name").item(0).getTextContent());
                    meetingRoom.setCode(mrElement.getElementsByTagName("code").item(0).getTextContent());
                    meetingRoom.setDescription(mrElement.getElementsByTagName("description").item(0).getTextContent());
                    meetingRoom.setCapacity(Integer.parseInt(mrElement.getElementsByTagName("capacity").item(0).getTextContent()));
                    meetingRoom.setHasVideoConference(Boolean.parseBoolean(mrElement.getElementsByTagName("hasVideoConference").item(0).getTextContent()));

                    meetingCentre.addMeetingRoom(meetingRoom);

                    NodeList reservations = mrElement.getElementsByTagName("reservation");

                    for (int k = 0; k < reservations.getLength(); k++) {
                        Node rNode = reservations.item(k);

                        Reservation reservation = new Reservation();
                        reservation.setMeetingRoom(meetingRoom);

                        Element rElement = (Element) rNode;
                        reservation.setDate(LocalDate.parse(rElement.getElementsByTagName("date").item(0).getTextContent()));
                        reservation.setTimeFrom(rElement.getElementsByTagName("timeFrom").item(0).getTextContent());
                        reservation.setTimeTo(rElement.getElementsByTagName("timeTo").item(0).getTextContent());
                        reservation.setExpectedPersonCount(Integer.parseInt(rElement.getElementsByTagName("expectedPersonsCount").item(0).getTextContent()));
                        reservation.setCustomer(rElement.getElementsByTagName("customer").item(0).getTextContent());
                        reservation.setNeedVideoConference(Boolean.parseBoolean(rElement.getElementsByTagName("videoConference").item(0).getTextContent()));
                        reservation.setNote(rElement.getElementsByTagName("note").item(0).getTextContent());

                        if (meetingRoom.getReservations() == null)
                            meetingRoom.setReservations(new ArrayList<>());
                        meetingRoom.addReservation(reservation);
                    }
                }

            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        System.out.println();

        System.out.println("**************************************************");
        System.out.println("Data was loaded correctly.");
        System.out.println("**************************************************");

        System.out.println();

        return meetingCentreList;
    }


    /**
     * Method to load the data from program’s default db (aka restore saved state).
     *
     * @return MeetingCentreList
     */
    public static List<MeetingCentre> restoreDataFromFile() {
        List<MeetingCentre> meetingCentreList = loadDataFromFile(defaultDbFile);

        System.out.println();

        System.out.println("**************************************************");
        System.out.println("Data was loaded correctly.");
        System.out.println("**************************************************");

        System.out.println();

        return meetingCentreList;
    }

    /**
     * Method to load data from specified file.
     *
     * @param input file name
     * @return meetingCentreList
     */
    private static List<MeetingCentre> loadDataFromFile(String input) {
        // MEETING_CENTRES,,,,,
        // Name,Code,Description,,,
        // ...
        // MEETING_ROOMS,,,,,
        // Name,Code,Description,Capacity,VideoConference,MeetingCentreCode

        List<MeetingCentre> meetingCentreList = new ArrayList<>();
        String csvLine;

        try (BufferedReader br = new BufferedReader(new FileReader(input))) {

            Boolean doingRoomRows = false;

            while ((csvLine = br.readLine()) != null) {
                String[] csvRow = csvLine.split(",");

                switch (csvRow[0].replace("\uFEFF", "")) {  // get rid of that pesky BOM
                    case "MEETING_CENTRES":
                        // fall through the first heading line
                        break;
                    case "MEETING_ROOMS":
                        // set flag and fall though the second heading line
                        doingRoomRows = true;
                        break;
                    default:
                        if (!doingRoomRows) {
                            // doing Meeting Centre
                            MeetingCentre centre = new MeetingCentre();
                            centre.setName(csvRow[0]);
                            centre.setCode(csvRow[1]);
                            centre.setDescription(csvRow[2]);
                            centre.setMeetingRooms(new ArrayList<>());
                            meetingCentreList.add(centre);
                        } else {
                            MeetingRoom room = new MeetingRoom();
                            room.setName(csvRow[0]);
                            room.setCode(csvRow[1]);
                            room.setDescription(csvRow[2]);
                            room.setCapacity(Integer.parseInt(csvRow[3]));
                            room.setHasVideoConference(csvRow[4].equals("YES"));
                            for (MeetingCentre mc : meetingCentreList) {
                                if (mc.getCode().equals(csvRow[5])) {
                                    room.setMeetingCentre(mc);
                                    mc.getMeetingRooms().add(room);
                                    break;
                                }
                            }
                        }
                }
            }
        } catch (FileNotFoundException e) {
            System.out.println("File " + input + " not found.");
        } catch (IOException e) {
            System.out.println("Cannot access " + input + " Check device accessibility.");
        }
        return meetingCentreList;
    }
}
