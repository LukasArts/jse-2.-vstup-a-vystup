package cz.unicorncollege.bt.utils;

import cz.unicorncollege.bt.model.Reservation;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.time.DateTimeException;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeParseException;
import java.util.List;
import java.util.regex.Pattern;

public class Choices {

    /**
     * Method to get the user choice from some list of options.
     *
     * @param choiceText String - Information text about options.
     * @param choices    List - list of options given to the user.
     * @return int - chosen option.
     */
    public static int getChoice(String choiceText, List<String> choices) {

        System.out.println(choiceText);
        printMenu(choices);

        System.out.print("> ");
        BufferedReader r = new BufferedReader(new InputStreamReader(System.in));

        try {
            return Integer.parseInt(r.readLine().trim());
        } catch (Exception e) {
            System.out.println("Invalid option.");
            return -1;
        }
    }

    public static void printMenu(List<String> choices) {
        for (int i = 0; i < choices.size(); i++) {
            System.out.println("  " + (i + 1) + " - " + choices.get(i));
        }
    }

    /**
     * Method to get the response from the user, typicaly some text or another data to fill in some object
     *
     * @param choiceText String - Info about what to enter.
     * @return String - user's answer.
     */
    public static String getInputInLength(String choiceText, int lengthMin, int lengthMax) {
        String result;
        System.out.print(choiceText);

        BufferedReader r = new BufferedReader(new InputStreamReader(System.in));
        while (true)
            try {
                result = r.readLine().trim();
                if (result.length() < lengthMin)
                    throw new IllegalArgumentException("Input too short. Enter at least " + lengthMin + " characters.");
                if (result.length() > lengthMax)
                    throw new IllegalArgumentException("Input too long. Enter at most " + lengthMax + " characters.");
                break;
            } catch (IllegalArgumentException e) {
                System.out.println(e.getMessage());
            } catch (Exception e) {
                System.out.println("Beware! Something unexpected happened!");
                e.printStackTrace();
            }

        return result;
    }

    public static String getInput(String choiceText) {
        return getInputInLength(choiceText, Integer.MIN_VALUE, Integer.MAX_VALUE);
    }

    public static int getNumberInRange(String choiceText, int rangeMin, int rangeMax) {
        int result;


        String input = "";

        while (true)
            try {
                input = getInput(choiceText);
                result = Integer.parseInt(input);
                if (result > rangeMax)
                    throw new NumberFormatException(' ' + input + " is too big (" + rangeMax + " is maximum).");
                if (result < rangeMin)
                    throw new NumberFormatException(' ' + input + " is too small (" + rangeMin + " is minimum).");
                break;
            } catch (NumberFormatException e) {
                System.out.println(input + " is not a valid number." + e.getMessage());
            }

        return result;
    }

    public static int getNumber(String choiceText) {
        return getNumberInRange(choiceText, Integer.MIN_VALUE, Integer.MAX_VALUE);
    }

    public static boolean getBoolean(String choiceText) {
        String input;

        do {
            input = getInput(choiceText);
        } while (!Pattern.matches("[aAyY1nN0]", input));

        return !Pattern.matches("[nN0]", input);
    }

    public static boolean parseBoolean(String s) {
        String input = s;

        while (!Pattern.matches("[aAyY1nN0]", input))
            input = getInput("Y/N? ");

        return !Pattern.matches("[nN0]", input);
    }

    public static LocalDate getDate(String choiceText) {
        LocalDate result;

        String input = "";

        while (true) {
            try {
                input = getInput(choiceText);
                result = LocalDate.parse(input);
                break;
            } catch (DateTimeParseException e) {
                System.out.println(input + " is not a date in YYYY-MM-YY format.");
            }
        }
        return result;
    }

    public static String getTime(String choiceText, List<Reservation> reservations) {
        // TODO: make class to use LocalTime instead of String for time information
        LocalTime result;

        String input = "";

        while (true) {
            try {
                input = getInput(choiceText);
                // if user did not enter anything return empty string and let caller handle this case
                if (input.equals(""))
                    return input;
                result = LocalTime.parse(input);

                // check collision
                if (reservations != null && reservations.size() != 0) {
                    for (Reservation r : reservations) {
                        if (result.isAfter(LocalTime.parse(r.getTimeFrom())) && result.isBefore(LocalTime.parse(r.getTimeTo())))
                            throw new IndexOutOfBoundsException();
                    }
                }

                break;
            } catch (DateTimeParseException e) {
                System.out.println(input + " is not a date in HH:MM format.");
            } catch (IndexOutOfBoundsException e) {
                System.out.println(input + " collides with existing reservation.");
            }
        }
        return result.toString();
    }

    public static String getTimeAfter(String choiceText, String time, List<Reservation> reservations) {
        LocalTime result;

        String input = "";

        while (true) {
            try {
                input = getTime(choiceText, reservations);
                // if user did not enter anything return empty string and let caller handle this case
                if (input.equals(""))
                    return input;
                if ((result = LocalTime.parse(input)).isAfter(LocalTime.parse(time)))
                    break;
                else
                    throw new DateTimeException("Time should be after " + time);
            } catch (DateTimeException e) {
                System.out.println(e.getMessage());
            }
        }
        return result.toString();
    }

    public static String showMenuGetInput(List<String> choices, String prompt) {
        printMenu(choices);
        return getInput(prompt);
    }

    public static UserInput getOptionAndCode(String input) {
        int option;
        String code;

        try {
            option = input.contains("-") ?
                    Integer.parseInt(input.substring(0, 1)) :
                    Integer.parseInt(input);
            code = input.contains("-") ? input.substring(2, input.length()) : "";
        } catch (NumberFormatException e) {
            option = -1;
            code = null;
        }

        return new UserInput(option, code);
    }

    public static class UserInput {
        int option;
        String code;

        UserInput(int option, String code) {
            this.option = option;
            this.code = code;
        }

        public int getOption() {
            return option;
        }

        public String getCode() {
            return code;
        }

    }
}
