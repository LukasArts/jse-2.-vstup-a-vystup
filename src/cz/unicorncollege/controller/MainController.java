package cz.unicorncollege.controller;

import cz.unicorncollege.bt.utils.Choices;
import cz.unicorncollege.bt.utils.FileParser;

import java.util.ArrayList;
import java.util.List;

/**
 * Main controller class.
 * Contains methods to communicate with user and methods to work with files.
 *
 * @author UCL
 */
public class MainController {
    private MeetingController control;
    private ReservationController controlReservation;


    /**
     * Constructor of main class.
     */
    public MainController() {
        control = new MeetingController();
        control.init();

        controlReservation = new ReservationController(control);
    }


    /**
     * Main method, which runs the whole application.
     *
     * @param argv String[]
     */
    public static void main(String[] argv) {
        MainController instance = new MainController();
        instance.run();
    }

    /**
     * Method which shows the main menu and end after user chooses Exit.
     */
    private void run() {
        List<String> choices = new ArrayList<>();
        choices.add("List all Meeting Centres");
        choices.add("Add new Meeting Centre");
        choices.add("Reservations");
        choices.add("Import Data");
        choices.add("Export Data");
        choices.add("Exit and Save");
        choices.add("Exit");

        while (true) {
            switch (Choices.getChoice("Select an option: ", choices)) {
                case 1:
                    control.listAllMeetingCentres();
                    break;
                case 2:
                    control.addMeeMeetingCentre();
                    break;
                case 3:
                    controlReservation.showReservationMenu();
                    break;
                case 4:
                    control.setMeetingCentres(FileParser.importData());
                    control.listAllMeetingCentres();
                    break;
                case 5:
                    FileParser.exportDataToJSON(controlReservation);
                    break;
                case 6:
                    FileParser.saveData(control.toSaveString());
                    FileParser.saveData(control);
                    return;
                case 7:
                    if (control.globalModificationFlag)
                        if (Choices.getBoolean("Data were modified, do you want to save before exit? [y/n]: ")) {
                            FileParser.saveData(control.toSaveString());
                            FileParser.saveData(control);
                        }
                    return;
            }
        }
    }
}
