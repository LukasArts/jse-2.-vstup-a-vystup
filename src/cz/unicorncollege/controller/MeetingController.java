package cz.unicorncollege.controller;

import cz.unicorncollege.bt.model.MeetingCentre;
import cz.unicorncollege.bt.model.MeetingObject;
import cz.unicorncollege.bt.model.MeetingRoom;
import cz.unicorncollege.bt.utils.Choices;
import cz.unicorncollege.bt.utils.FileParser;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.regex.Pattern;

import static cz.unicorncollege.bt.utils.Choices.*;

public class MeetingController {
    final Comparator<MeetingCentre> byCode = Comparator.comparing(MeetingObject::getCode);
    boolean globalModificationFlag = false;
    private List<MeetingCentre> meetingCentres;

    /**
     * Method to initialize data from the saved datafile.
     */
    public void init() {

//        meetingCentres = FileParser.restoreDataFromFile();
        meetingCentres = FileParser.loadDataFromFile();
        System.out.println(shortMeetingCentreList());
    }

    /**
     * Method to list all meeting centres to user and give him some options what to do next.
     */
    public void listAllMeetingCentres() {

        System.out.println(shortMeetingCentreList());

        List<String> choices = new ArrayList<>();
        choices.add("Show Details of Meeting Centre with code:");
        choices.add("Edit Meeting Centre with code:");
        choices.add("Delete Meeting Centre with code:");
        choices.add("Go Back to Home");

        Choices.UserInput ui;

        while (true)
            switch ((ui = getOptionAndCode(showMenuGetInput(choices, "Choose option (including code after '-', example 1-M01): "))).getOption()) {
                case 1:
                    if (!checkMCCode(ui.getCode())) break;
                    showMeetingCentreDetails(ui.getCode());
                    break;
                case 2:
                    if (!checkMCCode(ui.getCode())) break;
                    editMeetingCentre(ui.getCode());
                    break;
                case 3:
                    if (!checkMCCode(ui.getCode())) break;
                    deleteMeetingCentre(ui.getCode());
                    break;
                case 4:
                    return;
                case -1:
                default:
                    System.out.println("Invalid option.");
            }

    }

    private boolean checkMCCode(String code) {
        if (code.equals("") || getMeetingCentreByCode(code) == null) {
            System.out.println("Invalid meeting centre code.");
            return false;
        }
        return true;
    }

    private boolean checkMRCode(String mcCode, String mrCode) {
        if (!checkMCCode(mcCode) || getMeetingRoomByCode(mcCode, mrCode) == null) {
            System.out.println("Invalid meeting room code.");
            return false;
        }
        return true;
    }

    Choices.UserInput getUserInput(List<String> choices) {
        printMenu(choices);
        Choices.UserInput ui;
        do {
            ui = getOptionAndCode("blah");
            if (getMeetingCentreByCode(ui.getCode()) == null)
                System.out.println("Meeting centre with code " + ui.getCode() + " not found.");
        } while (getMeetingCentreByCode(ui.getCode()) == null);
        return ui;
    }

    private String shortMeetingCentreList() {
        final String DESC_HEADER = "\t CODE           NAME\n";
        StringBuilder shortDesc = new StringBuilder();
        for (MeetingCentre mc : meetingCentres) {
            shortDesc.append(mc.shortDesc());
            shortDesc.append('\n');
        }
        return DESC_HEADER + shortDesc;

    }

    /**
     * Method to add a new meeting centre.
     */
    public void addMeeMeetingCentre() {
        // EBC Brno,EBC-MC_BRNO,Executive Briefing Centre - Brno,,,
        MeetingCentre meetingCentre = new MeetingCentre();
        meetingCentre.setName(getInput("Meeting centre name: "));
        meetingCentre.setCode(getInput("Meeting centre code: "));
        meetingCentre.setDescription(getInput("Meeting centre description: "));
        meetingCentre.setMeetingRooms(new ArrayList<>());
        meetingCentres.add(meetingCentre);
        System.out.println("New meeting centre " + meetingCentre.getName() + " added.");
        globalModificationFlag = true;
    }

    /**
     * Method to show meeting centre details by id.
     */
    public void showMeetingCentreDetails(String input) {
        System.out.println("\n  " + getMeetingCentreByCode(input).longDesc() + '\n');

        List<String> choices = new ArrayList<>();
        choices.add("Show meeting rooms");
        choices.add("Add meeting room");
        choices.add("Edit details");
        choices.add("Go Back");

        Choices.UserInput ui;

        while (true) {
            switch ((ui = getOptionAndCode(showMenuGetInput(choices, "Choose option (including code after '-', example 3-EBC-C7-MR:1_3): "))).getOption()) {
//            switch (Choices.getChoice("Select an option: ", choices)) {
                case 1: // show meeting rooms
                    System.out.println(getMeetingCentreByCode(input).listMeetingRooms());
                    break;
                case 2: { // add new meeting room
                    MeetingCentre mc = getMeetingCentreByCode(input);
                    MeetingRoom mr = new MeetingRoom();
                    mr.setName(getInput("Room name: "));
                    mr.setCode(getInput("Room code: "));
                    mr.setDescription(getInput("Room description: "));
                    mr.setCapacity(getNumber("Room capacity: "));
                    mr.setHasVideoConference(getBoolean("Is room equipped with video conference [y/n]: "));
                    mr.setMeetingCentre(mc);
                    mc.addMeetingRoom(mr);
                    System.out.println("Room " + mr.getName() + " added to " + getMeetingCentreByCode(input).getName());
                    globalModificationFlag = true;
                    break;
                }
                case 3: { // edit meeting room details
                    if (!checkMRCode(input, ui.getCode())) break;

                    MeetingRoom mr = getMeetingRoomByCode(input, ui.getCode());

                    System.out.println(mr.description());
                    String userInput = getInput("Room name [" + mr.getName() + "]: ");
                    if (!userInput.equals("")) mr.setName(userInput);
                    userInput = getInput("Room code [" + mr.getCode() + "]: ");
                    if (!userInput.equals("")) mr.setCode(userInput);
                    userInput = getInput("Room description [" + mr.getDescription() + "]: ");
                    if (!userInput.equals("")) mr.setDescription(userInput);
                    userInput = getInput("Room capacity [" + mr.getCapacity() + "]: ");
                    if (!userInput.equals("")) {
                        int capacity;
                        try {
                            capacity = Integer.parseInt(userInput);
                        } catch (NumberFormatException e) {
                            System.out.println(userInput + " is not a number.");
                            capacity = getNumber("Room capacity [" + mr.getCapacity() + "]: ");
                        }
                        mr.setCapacity(capacity);
                    }
                    userInput = getInput("Is room equipped with video conference [" + mr.isHasVideoConference() + "] [y/n]: ");
                    if (!userInput.equals("")) {
                        boolean hasVideoConference;
                        hasVideoConference = Pattern.matches("[aAyY1]", userInput) || !Pattern.matches("[nN0]", userInput) && getBoolean("Is room equipped with video conference [" + mr.isHasVideoConference() + "] [y/n]: ");
                        mr.setHasVideoConference(hasVideoConference);
                    }
                    userInput = getInput("Meeting centre code [" + getMeetingCentreByCode(input).getCode() + "]: ");
                    if (!userInput.equals("")) {
                        if (checkMCCode(userInput)) {
                            if (!mr.getMeetingCentre().getCode().equals(userInput))
                                mr.getMeetingCentre().removeMeetingRoom(mr);
                            mr.setMeetingCentre(getMeetingCentreByCode(userInput));
                            getMeetingCentreByCode(userInput).addMeetingRoom(mr);
                        } else {
                            do {
                                System.out.println(userInput + " is not an existing meeting centre code.");
                                userInput = getInput("Meeting centre code [" + getMeetingCentreByCode(input).getCode() + "]: ");
                            } while (getMeetingCentreByCode(userInput) == null);
                            if (!mr.getMeetingCentre().getCode().equals(userInput))
                                mr.getMeetingCentre().removeMeetingRoom(mr);
                            mr.setMeetingCentre(getMeetingCentreByCode(userInput));
                            getMeetingCentreByCode(userInput).addMeetingRoom(mr);
                        }
                    }
                    System.out.println("Meeting room " + mr.getName() + " modified.");
                    globalModificationFlag = true;

                    break;
                }
                case 4:
                    return;
                default:
                    System.out.println("Invalid option.");
                    break;
            }
        }
    }

    /**
     * Method to edit meeting centre data by id.
     */
    public void editMeetingCentre(String meetingCentreCode) {
        // EBC Classic 7,EBC-MC_C7,Executive Briefing Centre - Classic 7 Praha Holešovice - Jankovcova
        boolean editFlag = false;

        if (checkMCCode(meetingCentreCode)) {
            MeetingCentre meetingCentre = getMeetingCentreByCode(meetingCentreCode);
            System.out.println(meetingCentre.shortDesc());

            String userInput = getInput("Meeting centre name [" + meetingCentre.getName() + "]: ");
            if (!userInput.equals("")) {
                meetingCentre.setName(userInput);
                editFlag = true;
            }
            userInput = getInput("Meeting centre code [" + meetingCentre.getCode() + "]: ");
            if (!userInput.equals("")) {
                meetingCentre.setCode(userInput);
                editFlag = true;
            }
            userInput = getInput("Meeting centre description [" + meetingCentre.getDescription() + "]: ");
            if (!userInput.equals("")) {
                meetingCentre.setDescription(userInput);
                editFlag = true;
            }

            if (editFlag) {
                System.out.println("Meeting centre " + meetingCentre.getName() + " modified.");
                globalModificationFlag = true;
            } else
                System.out.println("Meeting centre " + meetingCentre.getName() + " not modified.");
        } else {
            System.out.println(meetingCentreCode + " is not valid meeting centre code.");
        }

    }

    /**
     * Method to delete by id
     */
    public void deleteMeetingCentre(String meetingCentreCode) {
        if (checkMCCode(meetingCentreCode)) {
            MeetingCentre meetingCentre = getMeetingCentreByCode(meetingCentreCode);
            for (Iterator<MeetingRoom> iterator = meetingCentre.getMeetingRooms().iterator(); iterator.hasNext(); ) {
                MeetingRoom meetingRoom = iterator.next();
                iterator.remove();
            }
            String meetingCentreName = meetingCentre.getName();
            meetingCentres.remove(meetingCentre);
            System.out.println("Meeting centre " + meetingCentreName + " deleted.");
            globalModificationFlag = true;
        } else {
            System.out.println(meetingCentreCode + " is not valid meeting centre code.");
        }
    }

    /**
     * Method to get all data to save in string format
     *
     * @return CSV in a String
     */
    public String toSaveString() {
        StringBuilder sb = new StringBuilder();
        List<MeetingRoom> meetingRoomList = new ArrayList<>();
        sb.append("MEETING_CENTRES,,,,,\n");
        for (MeetingCentre mc : meetingCentres) {
            sb.append(mc.toCSVRow());
            sb.append('\n');
            meetingRoomList.addAll(mc.getMeetingRooms());
        }
        sb.append("MEETING_ROOMS,,,,,\n");
        for (MeetingRoom mr : meetingRoomList) {
            sb.append(mr.toCSVRow());
            sb.append('\n');
        }
        return sb.toString();
    }

    public List<MeetingCentre> getMeetingCentres() {
        return meetingCentres;
    }

    public void setMeetingCentres(List<MeetingCentre> meetingCentres) {
        if (meetingCentres != null)
            this.meetingCentres = meetingCentres;
    }

    public List<MeetingCentre> getMeetingCentresSortedByCode() {
        if (meetingCentres != null) {
            List<MeetingCentre> result = meetingCentres;
            result.sort(byCode);
            return result;
        }
        return null;
    }

    private MeetingCentre getMeetingCentreByCode(String code) {
        for (MeetingCentre mc : meetingCentres) {
            if (mc.getCode().equals(code))
                return mc;
        }
        // not found
        return null;
    }

    private MeetingRoom getMeetingRoomByCode(String mcCode, String mrCode) {
        for (MeetingRoom mr : getMeetingCentreByCode(mcCode).getMeetingRooms()) {
            if (mr.getCode().equals(mrCode))
                return mr;
        }
        // not found
        return null;
    }
}
