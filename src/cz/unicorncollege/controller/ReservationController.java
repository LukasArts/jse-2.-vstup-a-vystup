package cz.unicorncollege.controller;

import cz.unicorncollege.bt.model.MeetingCentre;
import cz.unicorncollege.bt.model.MeetingRoom;
import cz.unicorncollege.bt.model.Reservation;
import cz.unicorncollege.bt.utils.Choices;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static cz.unicorncollege.bt.utils.Choices.*;

public class ReservationController {
    private MeetingController meetingController;
    private MeetingCentre actualMeetingCentre;
    private MeetingRoom actualMeetingRoom;
    private Date actualDate;
    /**
     * Constructor for ReservationController class
     *
     * @param meetingController loaded data of centers and its rooms
     */
    public ReservationController(MeetingController meetingController) {
        this.meetingController = meetingController;
        this.actualDate = new Date();
    }

    public MeetingController getMeetingController() {
        return meetingController;
    }

    /**
     * Method to show options for reservations
     */
    public void showReservationMenu() {
        if (meetingController.getMeetingCentres().isEmpty()) {
            System.out.println("There are no meeting centres in the system.");
            return;
        }

        List<String> choices = new ArrayList<>();

        // let them choose one of the loaded meeting centres
        for (MeetingCentre centre : meetingController.getMeetingCentres()) {
            choices.add(centre.getCode() + " - " + centre.getName());
        }

        // get the choice
        int chosenOption = Choices.getChoice("Choose the Meeting Centre: ", choices);
        // get the chosen meeting center
        actualMeetingCentre = meetingController.getMeetingCentres().get(--chosenOption);
        choices.clear();

        if (actualMeetingCentre.getMeetingRooms() == null || actualMeetingCentre.getMeetingRooms().size() == 0) {
            System.out.println("\nThere are no rooms in " + actualMeetingCentre.getName() + ".\n");
            // doing recursive call to allow user to change actual room
            showReservationMenu();
            return;
        }

        // display rooms from actual meeting center
        for (MeetingRoom room : actualMeetingCentre.getMeetingRooms()) {
            choices.add(room.getCode() + " - " + room.getName());
        }

        chosenOption = Choices.getChoice("Choose a room to create a reservation: ", choices);

        actualMeetingRoom = actualMeetingCentre.getMeetingRooms().get(--chosenOption);
        choices.clear();

        getItemsToShow();
    }

    private void editReservation() {
        boolean editFlag = false;
        List<String> reservationList = new ArrayList<>();

        List<Reservation> reservations = actualMeetingRoom.getSortedReservations();

        if (reservations == null || reservations.isEmpty()) {
            System.out.println("There are no reservations to edit.");
            return;
        }

        for (Reservation r : reservations)
            reservationList.add(r.shortDesc());

        int chosenOption = getChoice("Choose the reservation: ", reservationList);
        if (chosenOption == -1)
            return;

        Reservation r = reservations.get(--chosenOption);

        String userInput = getInput("Reservation date [" + r.getDate() + "]: ");
        if (!userInput.equals("")) {
            try {
                r.setDate(LocalDate.parse(userInput));
            } catch (DateTimeParseException e) {
                r.setDate(getDate("Date (YYYY-MM-DD): "));
            }
            editFlag = true;
        }
        userInput = getTime("Time from [" + r.getTimeFrom() + "]: ", actualMeetingRoom.getSortedReservationsByDate(actualDate));
        if (!userInput.equals("")) {
            r.setTimeFrom(userInput);
            editFlag = true;
        }
        userInput = getTimeAfter("Time to [" + r.getTimeTo() + "]: ", r.getTimeFrom(), actualMeetingRoom.getSortedReservationsByDate(actualDate));
        if (!userInput.equals("")) {
            r.setTimeTo(userInput);
            editFlag = true;
        }

        userInput = getInput("Expected person count (1-" + actualMeetingRoom.getCapacity() + ") [" + r.getExpectedPersonCount() + "]: ");
        if (!userInput.equals("")) {
            int count;
            try {
                count = Integer.parseInt(userInput);
            } catch (NumberFormatException e) {
                System.out.println(userInput + " is not a number.");
                count = getNumberInRange("Expected person count (1-" + actualMeetingRoom.getCapacity() + "): ", 1, actualMeetingRoom.getCapacity());
            }
            r.setExpectedPersonCount(count);
            editFlag = true;
        }

        userInput = getInput("Customer [" + r.getCustomer() + "]: ");
        if (!userInput.equals("")) {
            r.setCustomer(userInput);
            editFlag = true;
        }

        if (actualMeetingRoom.isHasVideoConference()) {
            userInput = getInput("Video conference [y/n] [" + String.valueOf(r.isNeedVideoConference()) + "]: ");
            if (!userInput.equals("")) {
                r.setNeedVideoConference(parseBoolean(userInput));
                editFlag = true;
            }
        }
        userInput = getInput("Note [" + r.getNote() + "]: ");
        if (!userInput.equals("")) {
            r.setNote(userInput);
            editFlag = true;
        }

        if (editFlag) {
            System.out.println("\nReservation " + r.shortDesc() + " modified.\n");
            meetingController.globalModificationFlag = true;
        } else
            System.out.println("\nReservation " + r.shortDesc() + " not modified.\n");
    }

    private void addNewReservation() {
        Reservation r = new Reservation();

        r.setMeetingRoom(actualMeetingRoom);
        r.setDate(Choices.getDate("Date (YYYY-MM-DD): "));
        r.setTimeFrom(Choices.getTime("Time from: ", actualMeetingRoom.getSortedReservationsByDate(actualDate)));
        r.setTimeTo(Choices.getTimeAfter("Time to: ", r.getTimeFrom(), actualMeetingRoom.getSortedReservationsByDate(actualDate)));
        r.setExpectedPersonCount(Choices.getNumberInRange("Expected person count (1-" + actualMeetingRoom.getCapacity() + "): ", 1, actualMeetingRoom.getCapacity()));
        r.setCustomer(Choices.getInputInLength("Customer: ", 2, 100));
        if (actualMeetingRoom.isHasVideoConference())
            r.setNeedVideoConference(Choices.getBoolean("Video conference [y/n]: "));
        r.setNote(Choices.getInputInLength("Note: ", 0, 300));

        if (actualMeetingRoom.getReservations() == null)
            actualMeetingRoom.setReservations(new ArrayList<>());
        actualMeetingRoom.getReservations().add(r);

        System.out.println("\nReservation " + r.shortDesc() + " added.\n");
        meetingController.globalModificationFlag = true;
    }

    private void deleteReservation() {
        List<String> choices = new ArrayList<>();

        List<Reservation> reservations;

        if ((reservations = actualMeetingRoom.getSortedReservationsByDate(actualDate)).isEmpty()) {
            System.out.println("There is no reservation in room " + actualMeetingRoom.getName() + " scheduled on " + getFormattedDate());
            return;
        }

        for (Reservation reservation : reservations) {
            choices.add(reservation.shortDesc());
        }

        int chosenOption = Choices.getChoice("Choose the Reservation: ", choices);

        Reservation reservationToDelete = reservations.get(--chosenOption);
        actualMeetingRoom.delReservation(reservationToDelete);

        System.out.println("\nReservation deleted.\n");
        meetingController.globalModificationFlag = true;
    }

    private void changeDate() {
        LocalDate localDate = Choices.getDate("Enter date (YYYY-MM-DD): ");

        actualDate = Date.from(localDate.atStartOfDay(ZoneId.systemDefault()).toInstant());

        getItemsToShow();
    }

    private void getItemsToShow() {
        listReservationsByDate(actualDate);

        List<String> choices = new ArrayList<>();
        choices.add("Edit Reservations");
        choices.add("Add New Reservation");
        choices.add("Delete Reservation");
        choices.add("Change Date");
        choices.add("Exit");

        while (true) {
            switch (Choices.getChoice("Select an option: ", choices)) {
                case 1:
                    editReservation();
                    break;
                case 2:
                    addNewReservation();
                    break;
                case 3:
                    deleteReservation();
                    break;
                case 4:
                    changeDate();
                    break;
                case 5:
                    return;
            }
        }
    }

    private void listReservationsByDate(Date date) {
        // list reservations
        List<Reservation> list = actualMeetingRoom.getSortedReservationsByDate(date);
        if (list != null && list.size() > 0) {
            System.out.println("");
            System.out.println("Reservations for " + getActualData());
            for (Reservation reserv : list) {
                System.out.println(reserv.getFormattedDate());
            }
            System.out.println("");
        } else {
            System.out.println("");
            System.out.println("There are no reservation for " + getActualData());
            System.out.println("");
        }
    }

    /**
     * Method to get formatted actual date
     *
     * @return
     */
    private String getFormattedDate() {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

        return sdf.format(actualDate);
    }

    /**
     * Method to get actual name of place - meeting center and room
     *
     * @return
     */
    private String getCentreAndRoomNames() {
        return "MC: " + actualMeetingCentre.getName() + " , MR: " + actualMeetingRoom.getName();
    }

    /**
     * Method to get actual state - MC, MR, Date
     *
     * @return
     */
    private String getActualData() {
        return getCentreAndRoomNames() + ", Date: " + getFormattedDate();
    }
}
